extends Node2D


const icon_path = "res://dialog/icons/"

func _process(delta):
	var ok = Input.is_action_pressed("ui_accept")
	var two = Input.is_action_pressed("ui_left")
	if $anim.is_playing():
		return
	if ok:
		print("play intro")
		play("continue", "agents")
	elif two:
		print("play next")
		play("next", "quest")
		

func play(mode, primary=null, secondary=null):
	if mode == "continue":
		$shadow.modulate.a = 0
		$active.modulate.a = 0
		var sprite = load(str(icon_path, primary,".png"))
		$active.texture = sprite
		$anim.play("enter")
	elif mode == "next":
		$shadow.modulate.a = 0
		$active.modulate.a = 0
		$shadow.texture = $active.texture
		var sprite = load(str(icon_path, primary,".png"))
		$active.texture = sprite
		$anim.play("next")
	elif mode == "burn":
		$anim.play("burn")
		
