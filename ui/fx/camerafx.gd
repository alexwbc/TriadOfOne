extends Camera2D


var target

var fade_in = true
var loaded_and_ready = false

const zoom_speed = Vector2(0.7, 0.7)
const zoom_limit = 0.3
var debug_countdown = 10


var caller#this is the node that will receive our command when fade to black
var return_cmd = []#this is the command we will send




func _enter_tree():
	raise()
	make_current()
	position = database.base_size/2
	$vignette.modulate.a = 0
	$black.modulate.a = 0



func _process(delta):
	if fade_in:
		var vignettemod = $vignette.modulate.a
		var curr_pos = position
		var fade_black = $black.modulate.a
		if vignettemod < 1:
			$vignette.modulate.a += 5*delta
		if zoom.x > zoom_limit:
			position = position.linear_interpolate(target,(1-zoom.x))
			zoom -= zoom_speed*delta
			$black.modulate.a += (zoom_speed.x-0.1)*delta
#			print($black.modulate.a)
		elif fade_black < 1:
			$black.modulate.a += 3*delta
		else:
			caller.call(return_cmd[0], return_cmd[1], self)
			fade_in = false
			$vignette.queue_free()
			$black.modulate.a = 1
			position = database.base_size/2

			
	elif loaded_and_ready:
		var ready_to_die = true
		if zoom.x < 1:
			ready_to_die = false
			zoom += zoom_speed*delta
		if $black.modulate.a > 0:
			ready_to_die = false
			$black.modulate.a -= 1*delta
			
		if ready_to_die:
			queue_free()
