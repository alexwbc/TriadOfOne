extends Panel

var speed = 1
var color = Color(0,0,0,0)


var fade_in = true


var caller = null
var return_cmd = null
var data = null
func _enter_tree():
	modulate = color
	rect_size = database.screen_size+database.base_size
	
#	database.call("test")


func _process(delta):
	if fade_in:
		modulate.a += speed*delta
		if modulate.a >= 1:
			fade_in = false
			if caller != null:
				if data != null:
					caller.call(return_cmd, data)
#					print("do the: " +str(return_cmd))
				else:
#					print("do the: " +str(return_cmd))
					caller.call(return_cmd)
			
	else:
		modulate.a -= speed*delta
		if modulate.a <= 0:
			queue_free()
