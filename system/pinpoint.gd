extends TextureButton

#process control
var mouse_hover = false
var running_action = false#highest priority

var timer = 0

const live_timer = 4
const fade_speed = 2
const opacy_unactive = 0.3

var data = null

var created = false
var label

#var my_name
#var my_icon
#var my_icon_kind
#var my_position = Vector2()
#var target_kind
#var target_id#npc,local,cinematic, world
#var fx


#needs:
#a name
#icon
#kind of target (npc, local, cinematic, world)
#id of target

#func fake_entrance():
#	my_name = "Fakius place"
#	my_icon = "00002"
#	my_icon_kind = "arrow"#npc, place, city
#	target_kind = "local"
#	target_id = 999
##	print("pos: " + str(randi()%database.base_size.x))
#	randomize()
#	my_position.x = randi()%int(database.base_size.x)
#	my_position.y = randi()%int(database.base_size.y)
	
func _enter_tree():
#	fake_entrance()
	label = $labelpos/name
	name = data["name"]
	label.text = data["name"]
#	texture_click_mask = load("res://ui/pinpoint/"+str(my_icon_kind)+"_mask.png")
#	texture_normal = load("res://ui/pinpoint/"+str(my_icon_kind)+"_"+str(my_icon)+".png")
	rect_global_position = Vector2(data["x"], data["y"])*database.base_size
	
	
	if data["kind"] == "travel":
#		print("data icon is: " +str(data["icon"]))
		var icon_name = str("res://maps/world/icons/",str(data["icon"]))
		texture_normal = load(str(icon_name,".png"))
		texture_click_mask =load(str(icon_name,"_m.png"))
	elif data["kind"] == "local":
		var icon_name = str("res://maps/world/icons/",str(data["icon"]))
		print("icon name is: " +str(icon_name))
		texture_normal = load(str(icon_name,".png"))
		texture_click_mask =load(str(icon_name,"_m.png"))


	
	
	if created:
		modulate.a = 1
		$newfx.emitting = true

		mouse_hover = false
		label.modulate.a = 0
#			var positionate = Vector2(i["x"], i["y"])*database.base_size
#			print("topos: " +str(positionate))
#			placing.rect_global_position = positionate



	set_process(false)





func _process(delta):
	if running_action:
		
		
		return
	var opacy = modulate.a
	if mouse_hover and database.world.active:
		$labelpos.global_position = get_global_mouse_position()
		if opacy < 1:
			modulate.a += fade_speed*delta
			label.modulate.a = opacy
		else:
			modulate.a = 1
			label.modulate.a = 1
			#set_process(false)
			return
	else:
		if opacy > opacy_unactive:
			modulate.a -= fade_speed*delta
			label.modulate.a = (opacy - opacy_unactive)
		else:
			modulate.a = opacy_unactive
			label.modulate.a = 0
			set_process(false)
			return


func _on_pinpoint_pressed():
	if !database.world.active:
		return
	mouse_hover = false
	database.world.active = false
	running_action = true
	set_process(true)
	if data["kind"] == "travel":
		database.main.request("world",data["target"])
		for i in range(database.world_elements.size()):
			var db_check = database.world_elements[i]
			if db_check["target"] == data["home"]:#target that points at me
				if db_check["home"] == data["target"]:#and also is on the target map
					database.world_elements[i]["active"] = true#so we set it true
	elif data["kind"] == "local":
		database.main.request("local",data["target"], rect_global_position)


func _on_pinpoint_mouse_entered():
	set_process(true)
	mouse_hover = true
	


func _on_pinpoint_mouse_exited():
	set_process(true)
	mouse_hover = false

