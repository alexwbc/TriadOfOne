extends Container

#var debug = "world"
var debug = "chat"
var debug_keyrel = false

var lang_list = ["en","zh","de","es","fr","it","ja","ko","th"] 

var fx_fade = preload("res://ui/fx/fade.tscn")
var fx_camera = preload("res://ui/fx/camerafx.tscn")

var s_db = 	{"ready":0
			,"busy":1
			,"freezing":2
#			main
			,"run_world":3
			,"run_chat":4
			,"run_localmap":5
			,"run_cinematic":6
#			other

			,"new_game":7
			}

var state = 0

func _enter_tree():
	database.main = self




func rotlang():#4debug, rotate langauges
	var lang_size = lang_list.size()
	var cur_lang = TranslationServer.get_locale()
	for i in range(lang_size):
		if lang_list[i] == cur_lang:#found our language...
			if i >= lang_size-1:#... but this is the last, so we set the first one
				TranslationServer.set_locale(lang_list[0])
			else:#...so we set the next one
				TranslationServer.set_locale(lang_list[i+1])
			return

func _input(event):
	if event.is_action("debug") and debug_keyrel:
		rotlang()
		debug_keyrel = false
	elif Input.is_action_just_released("debug"):
		debug_keyrel = true



func request(what, data, extra = null):
	if state != s_db["ready"]:
		print("request no accpeted becouse not ready")
		return
	if what == "world":
		state = s_db["busy"]
		run_world(data)
	elif what == "cinematic":
		state = s_db["busy"]
		run_cinematic(data)
	elif what == "new_game":
		state = s_db["busy"]
		run_new_game(data)
	elif what == "local":
#		print("opening local with: "+str(data))
		state = s_db["busy"]
		open_map(data, extra)
	elif what == "chat":
		$chat.request(data)
#		state = s_db["chat"]
#		$chat.request(data)
func open_map(id, target = null):
	if state != s_db["busy"]:
		return
	
	var camera_zoomfade = fx_camera.instance()
	if target != null:
		camera_zoomfade.target = target
	camera_zoomfade.caller = $map
	camera_zoomfade.return_cmd = ["refresh",id]
	state = s_db["run_localmap"]
	$map.add_child(camera_zoomfade)
	$map.raise()
	$map.show()
#	$map.refresh(id)
	


func cinematic_ends(id):#control for when cinematics ends, special measures are taken
	if id == 2:
		pass
#		run_world(100)
	elif id == 4:
#		print("onrun")
#		request("world", 100)
		request("cinematic", 2)
	
#func new_game(data=null):
#	print("new game cycle")
#	if state == s_db["ready"]:
#		print("here")
#		database.load_base_database()
#		var fx = fx_fade.instance()
#		fx.caller = self
#		fx.return_cmd = "new_game"
#		fx.data = "stuff"
#		fx.raise()
#		state = s_db["busy"]
#		add_child(fx)
#		print($story_control.get_script())
#		print($story_control.get_script())
#	elif state == s_db["new_game"]:
#		state = s_db["run_cinematic"]
#		$menu.hide()
#		$world.hide()
#		$cinematic.show()
#		$cinematic.raise()
#		$cinematic.run_scene(2)


func run_new_game(data=null):

	if state == s_db["busy"]:
		database.load_base_database()
		var fx = fx_fade.instance()
		fx.caller = self
		fx.return_cmd = "run_new_game"
		fx.data = "stuff"
		fx.raise()
		state = s_db["new_game"]
		add_child(fx)
#		print(database.story_chapters_db["new_game"]["control_script"])
#		var script = load(database.story_chapters_db["new_game"]["control_script"])
#		print("script is: " +str(script))

#		database.story_chapter = ["new_game", 0]
		
	elif state == s_db["new_game"]:
#		print("this indeed")
		state = s_db["run_cinematic"]
		$menu.hide()
		$world.hide()
		$map.hide()
		$cinematic.show()
#		$cinematic.raise()
#		$cinematic.run_scene(2)
		$cinematic.run_scene(2)
		database.bg_player.plug_music("music_dark_forest")
		$map100.hide()




func run_cinematic(scene):
	if state == s_db["busy"]:#call fx
#		print("going for cinematic")
		$cinematic.show()
		$cinematic.run_scene(scene)
		state = s_db["run_cinematic"]
#
#		var fx = fx_fade.instance()
#		fx.caller = self
#		fx.return_cmd = "run_cinematic"
#		fx.data = scene
#		fx.raise()
#		state = s_db["freezing"]
#		add_child(fx)
#	elif state == s_db["freezing"]:#fx return
#		state = s_db["run_cinematic"]
#
#		$chat.hide()
#		$cinematic.hide()
#		$map.hide()
#		$menu.hide()
#		$world.show()
#		$world.refresh(world_id)

func run_world(world_id):
#	print("world: " +str(world_id)+" is on the run")
#	print(state)
	if state == s_db["busy"]:#call fx
		var fx = fx_fade.instance()
		fx.caller = self
		fx.return_cmd = "run_world"
		fx.data = world_id
		fx.raise()
		state = s_db["freezing"]
		add_child(fx)
	elif state == s_db["freezing"]:#fx return
		state = s_db["run_world"]
		$chat.hide()
		$cinematic.hide()
		$map.hide()
		$map/background.hide()
		$menu.hide()
		$world.show()
		$world.refresh(world_id)
#	if state != s_db["ready"]:
#		print("another request, abort")
#		return
#	$chat.go_background()


func _ready():
#	print("wnated file is: " +str())
	TranslationServer.set_locale("en")
#	database.resize_array.append([$ref, ($ref.rect_size/database.base_size)])
#	database.resize_array.append([self, (self.rect_size/database.base_size)])
	var mysize = self.rect_size
	var result = mysize / database.base_size
	
#	print(result)
#	print($ref.rect_size)
	for i in $chat.graphic_elements:
		var size = i.rect_size/database.base_size
		var posit = i.rect_global_position/database.base_size
		database.resize_array.append([i, size, posit])
	for i in $cinematic.graphic_elements:
#		var posit = i.global_position/database.base_size
		var posit = i.global_position
		database.sprite_resize_array.append([i, posit])


	if debug == "chat":
		_debug_chat()
	elif debug == "cinematic":
		_debug_cinematic()
	elif debug == "map":
		_debug_map()
	elif debug == "menu":
		_debug_menu()
	elif debug == "world":
		_debug_world()

func switch_dialog_line(npc_to, id_to, switch_to):
#	print("sending switch")
#	print(npc_to)
	for n in range(database.npc.size()):
		if database.npc[n]["id"] == npc_to:
#			print("found id")
			for l in range(database.npc[n]["dialog_path"].size()):
				if database.npc[n]["dialog_path"][l]["level"] == id_to:
#					print("was: " +str(database.npc[n]["dialog_path"][l]["active"]))
					database.npc[n]["dialog_path"][l]["active"] = switch_to
#					print("now: " +str(database.npc[n]["dialog_path"][l]["active"]))
				break
		break
#	print(database.npc.find({"id":"cerene"}))
#	print("setting npc: " +str(npc))
#	print("line number: " +str(id))
#	print("as on/off: " +str(switch_to))


func database_switch(data):
#	print("received switch")
	var unpack = data.split(",")#the data unpacked in #sum strings
	if unpack[0] == "dialog_line":
		if unpack.size() != 4:#if data don't contain 4 strings, it must be broken
			return#abor
		#convert remaing data in something usable
		var npc_to = unpack[1]#string with id name of npc (ie: cerene)
		var line_id_to = int(unpack[2])#number of line (from string to number)
		var switch_to = bool(int(unpack[3]))#boolean on/of (from string, then number and then boolan on/off)
		switch_dialog_line(npc_to, line_id_to, switch_to)
		


func _debug_chat():
	request("chat","cerene")
#	$chat.show()
#	$cinematic.hide()
#	$map.hide()
#	$menu.hide()
#	$world.show()

func _debug_cinematic():
	$chat.hide()
	$cinematic.show()
	$map.hide()
	$menu.hide()
	$world.hide()

func _debug_map():
	$chat.hide()
	$cinematic.hide()
	$map.show()
	$menu.hide()
	$world.hide()

func _debug_menu():
	$chat.hide()
	$cinematic.hide()
	$map.hide()
	$menu.show()
	$world.hide()

func _debug_world():
	request("world", 100)
	return
	database.current_world = 100
	database.world.active = true
	$chat.hide()
	$cinematic.hide()
	$map.hide()
	$menu.hide()
	$world.show()



func _on_map100_pressed():
	request("world",3010)
	print("running world 100")
	$map100.hide()
	

func _on_map101_pressed():
	request("chat","guy")
#	var camerafx = load("res://ui/fx/camerafx.tscn").instance()
#	camerafx.target = $Sprite.global_position
#	$Sprite.queue_free()
#	add_child(camerafx)

func _on_map102_pressed():
	request("chat","cerene")
#	print("running world 102")
	

func _on_map103_pressed():
	for p in database.npc.size():
		for c in database.choices:
			if database.npc[p]["state"][c] != null:
				database.npc[p]["state"][c] = true

	

func _on_map104_pressed():
	request("world",104)
	print("running world 104")
	



func _on_Button_pressed():
	request("cinematic", 99)


func _on_Button2_pressed():
	request("cinematic", 1)


func _on_Button3_pressed():
	request("cinematic", 2)


func _on_Button4_pressed():
	new_game()
