extends Node2D

#conf
const icon_path = "res://maps/local/icons/"
const min_fade = 0.3
const fade_spd = 1


var data
#nodes
var localmap

var hover = false

#localppoint_#mapid#_#pinid#
func _enter_tree():
	localmap = database.map

	$name.text = str("localppoint_",localmap.current_id,"_",data["pin_id"])
#	print(data["icon"])
	var kind =data["kind"]
#	npc inn building exit
	if kind == "exit":
		$button.texture_normal = load(str(icon_path,"return_map.png"))
	else:
		$button.texture_normal = load(str(icon_path,data["icon"],".png"))



func _process(delta):
	if hover:
		modulate.a += fade_spd*delta
		if modulate.a >= 1:
			modulate.a = 1
			set_process(false)
	else:
		modulate.a -= fade_spd*delta
		if modulate.a <= min_fade:
			modulate.a = min_fade
			set_process(false)



func _on_button_mouse_entered():
	hover = true
	set_process(true)


func _on_button_mouse_exited():
	hover = false
	set_process(true)



func _on_button_focus_entered():
	hover = true
	set_process(true)



func _on_button_focus_exited():
	hover = false
	set_process(true)



func _on_button_pressed():
	print("got the message")
	if data["kind"] == "exit":
		localmap.close_map()
	
