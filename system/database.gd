extends Node

var ondebug = true


#config
const lang_list = ["en","zh","de","es","fr","it","ja","ko","th"] 
#var translations = []
var locales_db = {}


#main nodes:
var chat
var cinematic
var map
var menu
var world
var main
var bg_player

var current_world
var current_map
var current_npc


#main database
var world_map
var world_elements
var local_map
var generic_speech
var cinematic_db
var npc
var jukebox_lib = {}

const choices = ["awe","deceit","comply","fright","daunt","brace"]


#main story event control:
#const story_chapters_db = {
#	"new_game":{"control_script":"res://system/chapters/new_game.gd"}
#	,"chapter_one":{"control_node":"res://system/chapters/new_game.gd"}
#	}
#
#var story_chapter = [null,0]
#

#setting variables
var volume_music = 0
var volume_fx = -30

#database files
const db_files = [{"string":"world_map","file":"res://system/database/world_map.json"}
				,{"string":"npc","file":"res://system/database/npc.json"}
				,{"string":"world_elements","file":"res://system/database/world_elements.json"}
				,{"string":"cinematic_db","file":"res://system/database/cinematics.json"}
				,{"string":"local_map","file":"res://system/database/local_map.json"}
				,{"string":"generic_speech","file":"res://system/database/generic_speech.json"}
]
const load_save_db = [{"undefined_yet":"undefined_yet"}]

const path_music = "res://sounds/music/"


func load_game(slot):
	pass

func save_game(slot):
	pass



var wheater = "normal"

const base_size = Vector2(1024,600)

var screen_size
var resize_array = []
var sprite_resize_array = []

func _enter_tree():
	randomize()
	load_base_database()
	get_tree().connect("screen_resized", self, "_on_screen_resized")


func _ready():
	_on_screen_resized()
	


func _on_screen_resized():
	screen_size = OS.get_window_safe_area().size
#	menu.resize(OS.window_size)
#	cinematic.rect_min_size = screen_size
#	cinematic.rect_size = screen_size
#	print("size is: " +str(screen_size) + "rect is: " +str(cinematic.rect_size))
#	var ratio = base_size/screen_size
#	print(ratio)
#	var rat = base_size/screen_size
#	var old_ratio = base_size.x/base_size.y
#	var new_ratio = screen_size.x/screen_size.y
#	for i in resize_array:
#		i[0].rect_size = i[1]*screen_size
#		i[0].rect_global_position = i[2]*screen_size
#	for spr in sprite_resize_array:
#		spr[0].global_position.y = spr[1].y*(ratio.x)
#		spr[0].scale = Vector2(1,1)*ratio.x
#		print(spr[0].global_position.y)
		
#		spr[0] is sprite
#		spr[1] is position
		
#		spr[0].global_position.y = spr[1].y*rat.y
#		print(spr[1].y*rat)
#		print(str(base_size)+ " "+ str(screen_size))
#		print(spr[1])
#		spr[0].global_position.y = 100
#		var diff = screen_size -base_size
#		var pos = spr[1]*screen_size
#		spr[0].global_position = pos
#		print(str(pos) + " of screen size " + str(screen_size))
#		var size = spr[0].get_rect()
#		print(old_ratio- new_ratio)
#		var ratiodif = new_ratio -old_ratio
#		spr[0].scale = Vector2(1,1)*(1+ ratiodif)


func load_base_database():


	#load jukebox list (jukebox_lib)
	var dir = Directory.new()
	if dir.open(path_music) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if file_name.ends_with(".ogg.import"):
				var track = file_name.replace(".ogg.import", ".ogg")
#				print("name is: "+str(file_name.get_basename()))
				jukebox_lib[track.get_basename()] =  str(path_music+track)
			file_name = dir.get_next()
			
#		print(jukebox_lib)
	else:
		print("An error occurred when trying to access the path.")
	#loading the world maps in world_map
	


	for i in db_files:
		var file = File.new()
		file.open(i["file"], file.READ)
		set(i["string"], parse_json(file.get_as_text()))
		file.close()



	for i in lang_list:
		var new_translation = Translation.new()
		new_translation.locale = i
		locales_db[i] = new_translation

	for i in range(cinematic_db.size()):#initialize language database for cinematics.
		var scene = cinematic_db[i]
		for l in range(scene["lines"].size()):
#			var unique_id = cinematic_scene_line
			var line = scene["lines"][l]
			var unique_id = "cinematic_" +str(scene["id"])+"_"+str(l)
#			print(unique_id)
			for locale in lang_list:
				if line["text"][locale] != "":
					locales_db[locale].add_message(unique_id, line["text"][locale])


	for i in range(local_map.size()):#initialize language database for pinpoints on local maps. localppoint_#mapid#_#pinid#
		var contents = local_map[i]["content"]
		var mapid = local_map[i]["id"]
#		print("mapid is: " +str(mapid))
		for c in contents:
			var pinid = c["pin_id"]
			var unique_id = str("localppoint_", mapid, "_",pinid )
			for locale in lang_list:
				if c["name"][locale] != "":
					var text = c["name"][locale]
					locales_db[locale].add_message(unique_id, text)
#					print(unique_id)


	for i in range(npc.size()):
		var npc_check = npc[i]
		for locale in npc_check["intro"]:
			var line = npc_check["intro"][locale]
			#var unique_id = cerene_intro
			var unique_id = str(npc_check["id"])+"_intro"
			locales_db[locale].add_message(unique_id, line)

		var dialogs = npc_check["dialog_path"]
		for lvl in range(dialogs.size()):
			var unique_id = str(npc_check["id"])+"_"+str(dialogs[lvl]["level"])
			var right_text = dialogs[lvl]["right_text"]
			var wrong_text = dialogs[lvl]["wrong_text"]
			for locale in lang_list:
				if right_text[locale] != "":
					locales_db[locale].add_message(str(unique_id+"_r"), right_text[locale])
				if wrong_text[locale] != "":
					locales_db[locale].add_message(str(unique_id+"_w"), wrong_text[locale])

	for _class in generic_speech:
		
		var name_class = _class["class"]
		
		for locale in lang_list:
			if _class[locale] != "":
				print(_class[locale])
				var c_uuid = str("class_name_",name_class)
				locales_db[locale].add_message(str(c_uuid),_class[locale])



		for category in _class["category"]:
			var cat = category["category"]
			for line in category["text"].size():
				var text = category["text"][line]
				for locale in lang_list:
					if text[locale] != "":
						var uuid = str("generic_",name_class,"_",cat,"_",line)
#						print(uuid)
						locales_db[locale].add_message(str(uuid),text[locale])
#					print(text[locale])


	for i in locales_db:
		TranslationServer.add_translation(locales_db[i])

