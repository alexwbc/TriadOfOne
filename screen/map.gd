extends Container


#pintpoint name scheme: localppoint_#mapid#_#pinid#

var current_data
var current_id
var current_world
var content

#nodes
var local_pinpoint


const path = "res://maps/local/"


func _enter_tree():
	database.map = self
	local_pinpoint = preload("res://system/localpin.tscn")

func refresh(id, waiting_node = null):
	$background.show()
	var pointcleanup = $points.get_children()
	current_id = id
	if pointcleanup.size() >= 1:
		for i in pointcleanup:
			i.queue_free()
	for i in database.local_map:
		if i["id"] == str(id):
			current_data = i
			content = i["content"]
#	print(content)
	var background = current_data["background"]
	if background == "":
		background = id
	$background.texture = load(str(path,background,".png"))
	var bg_size = $background.texture.get_size()
	print("print size: " +str(bg_size))
	for i in content:
		if i["active"]:#this content is active, place on map
			var placepin = local_pinpoint.instance()
			var pos = Vector2(i["x"],i["y"])
			pos *= bg_size
			placepin.global_position = pos
			placepin.data = i
			$points.add_child(placepin)
	if waiting_node != null:
		waiting_node.loaded_and_ready = true


func close_map():#local map close,return to world map
	var pointcleanup = $points.get_children()
	if pointcleanup.size() >= 1:
		for i in pointcleanup:
			i.queue_free()
	var main = database.main
	main.state = main.s_db["ready"]
	main.request("world",database.current_world)


#	state != s_db["ready"]
#	main.state = main.s_db["run_world"]
#	database.world.active = true
	
func disable():
	pass
