extends Container


var buttons = {}
var count = 1
var audio_fx = 	{"menuswipe":"res://sounds/ui_sfx/sword-knife-clash-19.wav"
#				,"menuswippe":"res://sounds/ui_sfx/handleSmallLeather2.ogg"
				}

const fade_speed = 5
const fade_limit = 0.3

var testfx
func _enter_tree():
	database.menu = self
	buttons = 	{1:[$btn_new,false]
				,2:[$btn_cont, false]
				,3:[$btn_setting, false]
				,4:[$btn_quit, false]
#				,4:[$btn_cont, false]
				}#,$btn_setting,$btn_quit]

#	$backdrop.rect_min_size.y = 100
#	print("list")
	for a in audio_fx:#convert the textual content in audio_fx into actual audio node with sounds
		

#		var node = ClassType.new()
#		node.set_name("node")
#		add_child(node)
#		var stream = load(audio_fx[a])#load the binary content
		var AudioNode = AudioStreamPlayer2D.new()#create new audio stream
		AudioNode.name = a#give it the name
		AudioNode.stream = load(audio_fx[a])#place the loaded binary into stream
		audio_fx[a] = AudioNode
		
#		audio_fx[a].stream = preload(AudioNode)
#		audio_fx[a].play()
#		print(audio_fx[a])

#	print("listend")


func _ready():
	for n in buttons.size():
		buttons[n+1][0].get_node("fx").set_emitting(false)
#		buttons[n+1][0].modulate.a = 1




func resize(size):
	print("ok, got it:" + str(size))

func _process(delta):
	var opa = buttons[count][0].modulate.a
	if opa < fade_limit:
		opa = fade_limit
	if buttons[count][1] and (count == 1):#focus
		if opa < 1:
			buttons[count][0].modulate.a += fade_speed*delta
	else:#fade away
		if opa > fade_limit:
			buttons[count][0].modulate.a -= fade_speed*delta
	if count >= buttons.size():
		count = 1
	else:
		count += 1

func play_audio(audio):
	var new_child = audio_fx[audio].duplicate()
	new_child.connect("finished", new_child, "queue_free")
	new_child.position = get_global_mouse_position()
	new_child.volume_db = database.volume_fx
	new_child.play()
	$audio.add_child(new_child)


func _on_btn_new_mouse_entered():
	play_audio("menuswipe")
	$btn_new/fx.set_emitting(true)
	buttons[1][1] = true
	count = 1


func _on_btn_new_mouse_exited():
	$btn_new/fx.set_emitting(false)
	buttons[1][1] = false



func _on_btn_cont_mouse_entered():
	play_audio("menuswipe")
	$btn_cont/fx.set_emitting(true)
	buttons[2][1] = true
	count = 2


func _on_btn_cont_mouse_exited():
	$btn_cont/fx.set_emitting(false)
	buttons[2][1] = false


func _on_btn_setting_mouse_entered():
	play_audio("menuswipe")
	$btn_setting/fx.set_emitting(true)
	buttons[3][1] = true
	count = 3

func _on_btn_setting_mouse_exited():
	$btn_setting/fx.set_emitting(false)
	buttons[3][1] = false



func _on_btn_quit_mouse_entered():
	play_audio("menuswipe")
	$btn_quit/fx.set_emitting(true)
	buttons[4][1] = true
	count = 4



func _on_btn_quit_mouse_exited():
	$btn_quit/fx.set_emitting(false)
	buttons[4][1] = false



func _on_btn_new_pressed():
	database.main.request("new_game", "")
