extends AudioStreamPlayer


const default_music_stop = 300#hard limit, after 5 minutes music stop (if otherwise specified)
const music_path = "res://sounds/music/"
const volume_fade_spd = 10
const volume_fade_lmt = -30
var count_down = 0
var jukebox = {}
var stop_event
var shutdown_music = true
func _enter_tree():
#	print("music jubox is inside")
	database.bg_player = self
	var music_folder = Directory.new()
	if music_folder.open(music_path) == OK:
#		print("looking for music")
		music_folder.list_dir_begin()
		var file_name = music_folder.get_next()
		while (file_name != ""):
#			print("file name is: " +str(file_name))
			if file_name.ends_with(".ogg"):
#				print("music found: " +str(file_name))
				jukebox[file_name.get_basename()] =  str(music_path+file_name)
			file_name = music_folder.get_next()
	else:
		print("error witht he player, folder path is wrong")

func _process(delta):
	if !playing:
		set_process(false)
#		print("not playing" +str(delta))
		return

	if shutdown_music:
		volume_db -= volume_fade_spd*delta
		if volume_db <= volume_fade_lmt:
			stop()

	elif stop_event == null:
#		print("count down is: " +str(count_down))
		count_down -= delta
		if count_down <= 0:
			shutdown_music = true

func plug_music(track, stop_thing = null):
	if !database.jukebox_lib.has(track):
		print("error with tracks")
		return

	volume_db = database.volume_music
	stream = load(database.jukebox_lib[track])
	play()
	set_process(true)
	if stop_thing == null:
		count_down = default_music_stop
	stop_event = stop_thing
	shutdown_music = false


func plug_music_bk(track, stop_thing = null):
	if !jukebox.has(track):
		print("error with tracks")
		return

	volume_db = database.volume_music
	stream = load(jukebox[track])
	play()
	set_process(true)
	if stop_thing == null:
		count_down = default_music_stop
	stop_event = stop_thing
	shutdown_music = false