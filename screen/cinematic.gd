extends Container

var graphic_elements = []
var scene = null

const fade_opacity = 0.4
const typewriter_speed = [30, 100]
var typewriter_cursor = 0

var clean_mouse = false
var clean_key = false

const s_db = 	{"ready":0
				,"typing":1
				,"waiting_input":2
				,"next_line":3
				,"shut_down":4
				}

const pt_fld = "res://cinematics/actors/"
const pt_ext = ".png"

var status = 0
var current_line = 0
var scene_id = 0
var slot = {}
func _enter_tree():

	database.cinematic = self
	slot["a"] = [$slotA, "portait_a"]
	slot["b"] = [$slotB, "portait_b"]
	slot["c"] = [$slotC, "portait_c"]
	slot["d"] = [$slotD, "portait_d"]
	setup()


func _ready():
	set_process(false)

func setup():
	graphic_elements = [$slotA, $slotB, $slotC, $slotD ]

func _process(delta):
	if status == s_db["ready"]:
		return
	var mouse_press = Input.is_mouse_button_pressed(1)
	var ui_accept = Input.is_action_pressed("ui_accept")

	if status == s_db["typing"]:
		var speed = typewriter_speed[0]
		if mouse_press or ui_accept:
			speed = typewriter_speed[1]

		typewriter_cursor += speed*delta
		$dialog/text.visible_characters = typewriter_cursor
		if typewriter_cursor >= $dialog/text.get_total_character_count():
			status = s_db["waiting_input"]
			clean_key = false
			clean_mouse = false
	elif status == s_db["waiting_input"]:
		var wait = true
		if !clean_mouse and !mouse_press:
			clean_mouse = true
		elif clean_mouse and mouse_press:
			wait = false
		if !clean_key and !ui_accept:
			clean_key = true
		elif ui_accept and clean_key:
			wait = false
			
		if !wait:
			current_line += 1
			if current_line >= scene["lines"].size():#it was the last line. shutdown
				status = s_db["shut_down"]
				return
			else:
				line_switch()

	elif status == s_db["shut_down"]:
		modulate.a -= 2*delta
		if modulate.a <= 0:
			status = s_db["ready"]
			hide()
			var main = database.main
			if main.state == main.s_db["run_cinematic"]:
				main.state = main.s_db["ready"]
			main.cinematic_ends(scene_id)
			set_process(false)


func run_scene(new_scene):
	print("running scene")
	set_process(true)
	scene_id = new_scene
	for i in database.cinematic_db:
		if int(i["id"]) == new_scene:
			scene = i
			if i["activator"] != "":
				database.main.database_switch(i["activator"])
			break
	for i in graphic_elements:
		i.modulate.a = fade_opacity
		i.hide()
	current_line = 0
#	$dialog/text.percent_visible = 0
	typewriter_cursor = 0
	$dialog/text.visible_characters = 0
	$dialog/text.text = ""
	modulate.a = 1
	line_switch()
	set_process(true)

func line_switch():
	var script_line = scene["lines"][current_line]
	var background = ""
	if script_line.has("background"):
		background = script_line["background"]
	var focus = script_line["focus_slot"]
	for i in slot:
		var portait_req = script_line[slot[i][1]]
		if portait_req != "":
			slot[i][0].show()
			slot[i][0].texture = load(str(pt_fld,portait_req,pt_ext))
		else:
			slot[i][0].texture = null
			slot[i][0].hide()
		if i == focus:
			slot[i][0].show()
			slot[i][0].modulate.a = 1
		else:
			slot[i][0].modulate.a = fade_opacity
	if focus != "":
		slot[focus][0].show()
	if background != "":
		if background == "clean":
			$background.texture = null
		else:
			$background.texture = load("res://cinematics/bg/"+ str(background)+".png")
#		print("background is set")
	else:
		pass
#		print("nobgrequest here")
		
	$dialog/text.text = "cinematic_"+str(scene_id)+"_"+str(current_line)
	$dialog/text.visible_characters = 0
	typewriter_cursor = 0
	status = s_db["typing"]

func disable():
	pass
