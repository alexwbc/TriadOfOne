extends Container

const portait_path = "res://dialog/npc/"

var graphic_elements = []


var generic_speech



var active_npc_db
var npc_id
var idx_npc
var idx_path

const fade_spd = 3#speed in which buttons fade out or in

const face_delay = 1#time to wait before
var face_count = 1


#nodes
onready var buttons = 	{"deceit":$deceit
						,"comply":$comply
						,"fright":$fright
						,"awe":$awe
						,"daunt":$daunt
						,"brace":$brace
						}

onready var icon = $icon
#typewriter:
const typewriter_speed = [30, 100]
var typewriter_cursor = 0
var typewriter_on = false

var base_portait

const s_db = 	{"start":0
				,"chatting":1
				,"waiting_reply":2
				,"monologue":3
				,"waiting_clickend":4
				,"retry":5
				,"shut_down":6
				#sub states
				,"typewriting":10
				,"typewritend":11
				,"typewritoff":12
				}

var state = 0
var sub_state

#state
#
# - npc speaking {during half dialogue return portait normal}
# - waiting input: set-active button; wait
# - button wrong - set dialogue wrong, set portait wrong : go monologue
# - button right - set dialogue right, set portait right.
#
#check if next level aviable: 
#					yes: npc speaking
#					no: monologue

# - monologue: npc speak
# - wait clickshutdown

#to do put database in active npc

#enter
#if player unactive, play last wrong
#if player active
#		if player level 0: play welcome
#		else: play last right

#uuid for dialogues
#first meet intro: npc_id,"_intro" (cerene_intro)
#right: cerene_0_r
#wrong: cerene_0_w

#generic_villager_busy
#generic_class_category

#class_name_[class_name]

var mouse_press
var mouse_pressed
var ui_accept
var ui_accepted

func setup():
	graphic_elements = [$Panel, $Panel2, $Panel3, $Panel4]
	

func _enter_tree():
	sub_state = s_db["typewritoff"]
	setup()
	database.chat = self


func typewriter(delta):
	var text = $reply/speech
	var speed = typewriter_speed[0]
	if mouse_press or ui_accept:
		speed = typewriter_speed[1]
	typewriter_cursor += speed*delta
	if text.percent_visible < 1:#end?
		text.visible_characters = typewriter_cursor
	else:
		text.percent_visible = 1
		sub_state = s_db["typewritend"]

func _process(delta):

	mouse_press = Input.is_mouse_button_pressed(1)
	ui_accept = Input.is_action_pressed("ui_accept")


	if state == s_db["chatting"]:
		var opa = $deceit.modulate.a
		if opa < 1:
			opa += fade_spd*delta
			for i in buttons:
				buttons[i].modulate.a = opa
			$goodbye.modulate.a = opa
		if sub_state == s_db["typewritoff"]:#initiate typewriter
			$reply/speech.percent_visible = 0

			sub_state = s_db["typewriting"]
			return
		elif sub_state == s_db["typewriting"]:#typewrite until reach end
			typewriter(delta)
		elif sub_state == s_db["typewritend"]:#cleanup
			if mouse_press or ui_accept:
				print("not going in to choices")
				return
			print("gone to chose"+str(mouse_press," and ", ui_accept))
			typewriter_cursor = 0
			sub_state = s_db["typewritoff"]
			state = s_db["waiting_reply"]
			set_buttons()
			return
	elif state == s_db["monologue"]:
		
		
		var opa = $deceit.modulate.a
		if opa > 0:
			opa -= fade_spd*delta
			for i in buttons:
				buttons[i].modulate.a = opa
			$goodbye.modulate.a = opa
		var text = $reply/speech


		if sub_state == s_db["typewritoff"]:#initiate typewriter
			$reply/speech.percent_visible = 0

			sub_state = s_db["typewriting"]
			return
		elif sub_state == s_db["typewriting"]:#typewrite until reach end
			typewriter(delta)
		elif sub_state == s_db["typewritend"]:#cleanup
			typewriter_cursor = 0
			state = s_db["waiting_clickend"]
			mouse_pressed = mouse_press
			ui_accepted = ui_accept
			set_buttons()
	elif state == s_db["retry"]:
		
		
		var opa = $deceit.modulate.a
		if opa > 0:
			opa -= fade_spd*delta
			for i in buttons:
				buttons[i].modulate.a = opa
			$goodbye.modulate.a = opa
		var text = $reply/speech


		if sub_state == s_db["typewritoff"]:#initiate typewriter
			$reply/speech.percent_visible = 0

			sub_state = s_db["typewriting"]
			return
		elif sub_state == s_db["typewriting"]:#typewrite until reach end
			typewriter(delta)

			mouse_pressed = mouse_press
			ui_accepted = ui_accept
		elif sub_state == s_db["typewritend"]:#cleanup

			print("wait for clean")

			if (!mouse_pressed and mouse_press) or (!ui_accepted and ui_accept):
				print("cleanning")
				sub_state = s_db["typewritoff"]
				state = s_db["start"]
				typewriter_cursor = 0
				set_buttons()
				run_action()
				return
			mouse_pressed = mouse_press
			ui_accepted = ui_accept



#			if mouse_press or ui_accept:
#				sub_state = s_db["typewritoff"]
#				state = s_db["start"]
#				typewriter_cursor = 0
#				set_buttons()
#				run_action()

	elif state == s_db["waiting_clickend"]:

		if !mouse_pressed and mouse_press:
			end_speak()
			return
		if !ui_accepted and ui_accept:
			end_speak()
			return
		mouse_pressed = mouse_press
		ui_accepted = ui_accept
	elif state == s_db["waiting_reply"]:
		if face_count >0:
			face_count -= delta
		elif face_count > -1:
			set_npc_face("normal")
			face_count = -2


func end_speak():
	database.main.state = database.main.s_db["ready"]
	state = s_db["shut_down"]
	hide()


func set_buttons():
	var npc_state = database.npc[idx_npc]["state"]
	for i in buttons:
		buttons[i].disabled = !npc_state[i]

func set_npc_face(face):
	if face == "normal":
		$"npc-sprite".texture = base_portait
		return
	var portait = load(str(portait_path,npc_id,"_",face,".png"))
	if portait == null:
		$"npc-sprite".texture = base_portait
	else:
		$"npc-sprite".texture = portait

func run_action():
	typewriter_cursor = 0
	sub_state = s_db["typewritoff"]
	var npc = database.npc[idx_npc]
	var npc_state = npc["state"]
	var dialog_line = null
	for i in npc["dialog_path"]:
		if i["level"] == npc_state["level"]:
			dialog_line = i
			break
	if dialog_line == null:

		failsafe_exit()
		return
	if !npc_state["active"]:#npc don't want to talk
		if npc_state["level"] == 0:
			pass#RUN MONOLOGUE FOR NPC CLASS GENERIC BUSY TALK
		else:
			pass#RUN MONOLOGUE about....dunno, not decided yet
	else:#npc is open to talk
		print("npc open to talk")
		var choice_options = [npc_state["deceit"],npc_state["comply"],npc_state["fright"],npc_state["awe"],npc_state["daunt"],npc_state["brace"]]
		if choice_options == [false,false,false,false,false,false]:#but we have burned all option... so, nope. its not open to talk
#			print("npc in failstate")
			state = s_db["monologue"]
			$reply/speech.visible_characters = 0
			icon.play("burn")
#			typewriter_cursor = 0
			$reply/speech.text = str(npc_id,"_",npc["state"]["level"],"_w")
			set_npc_face(dialog_line["wrong_face"])
#			$"npc-sprite".texture = load(str(portait_path,npc_id
		else:
			#check if there's a next one aviable
			var next_available = false
			for i in npc["dialog_path"]:
				if i["level"] == (npc["state"]["level"]+1):
					next_available = true
					break
			if next_available:#after this one 
				state = s_db["chatting"]
			else:
				state = s_db["monologue"]
				
			$reply/speech.visible_characters = 0
			typewriter_cursor = 0
#			$"npc-sprite".texture = base_portait
			if npc_state["level"] == 0:#run intro with dialogue
				$reply/speech.text = str(npc_id,"_intro")
				set_npc_face("normal")
				$name.text = str("class_name_",database.npc[idx_npc]["class"])
#				print("class is: " +str(database.npc[idx_npc]["class"]))
			else:#run last good with dialogue
#				print("class is: " +str(database.npc[idx_npc]["class"]))
				$reply/speech.text = str(npc_id,"_",npc["state"]["level"]-1,"_r")
				$name.text = str(database.npc[idx_npc]["name"])
				set_npc_face(dialog_line["right_face"])
				face_count = face_delay
				pass






func request(who):
	#flush db
	state = s_db["start"]
	var npc_class
	for i in database.npc.size():
		if database.npc[i]["id"] == who:
			idx_npc = i
			npc_id = who
			npc_class = database.npc[i]["class"]
			#
#			print(str(portait_path,who,"_normal.png"))
			base_portait = load(str(portait_path,who,"_normal.png"))
			break
	
	generic_speech = {}
	for _class in database.generic_speech:
		if _class["class"] == npc_class:
			for cat in _class["category"]:
				generic_speech[cat["category"]] = cat["text"].size()
				#generic_villager_busy
#				print("gen: " +str(generic_speech))
#			print("found at: " +str(_class))
#			print(
			break
	var cur_npc = database.npc[idx_npc]
	
	var level = cur_npc["state"]["level"]
	for line in cur_npc["dialog_path"]:
		if line["level"] == cur_npc["state"]["level"]:
			icon.play("continue", line["icon"])
#			print(line["icon"])
	
	for i in buttons:
		buttons[i].modulate.a = 0
	
	show()
	raise()
	run_action()

	#check if active
#	if database.npc[idx_npc]["state"]["active"]:
#		var level = database.npc[idx_npc]["state"]["level"]
#		if level == 0:
#			$reply/speech.text = str(npc_id,"_intro")
#			$reply/speech.visible_characters = 0
#			$"npc-sprite".texture = base_portait
#			$name.text = str("npc_genericname_",database.npc[idx_npc]["class"])
#
#
#			for di in database.npc[idx_npc]["dialog_path"].size():
#				if database.npc[idx_npc]["dialog_path"][di]["level"] == level:
#					idx_path = di
#
#			var next = level+1
#			if next <= database.npc[idx_npc]["dialog_path"].size():
#				if database.npc[idx_npc]["dialog_path"][next]["active"]:
#					print("the next one is active")
#					print("send dialogue and return")
#					return
#			print("send monologue and return")
#			return
#
#		else:
#			pass#run lastest "okey"
#		print(database.npc[npc_idx]["state"]["active"])
#	if main.state == main.s_db["chat"]:
#		pass

func chose(choice):
	if state != s_db["waiting_reply"]:
		return
	if choice == "goodbye":
		
		state = s_db["monologue"]
		$reply/speech.visible_characters = 0
		typewriter_cursor = 0
		$reply/speech.text = str("generic_",database.npc[idx_npc]["class"],"_goodbye_",randi() % generic_speech["goodbye"])
		set_npc_face("normal")

		return
	var cur_npc = database.npc[idx_npc]
	var npc_state = cur_npc["state"]
	var cur_dialog_path = null
	
	for di in cur_npc["dialog_path"].size():#find the current dialog and put it into cur_dialog_path
		if npc_state["level"] == cur_npc["dialog_path"][di]["level"]:
			cur_dialog_path = cur_npc["dialog_path"][di]
			break
	if cur_dialog_path == null:
		print("request dialog is missing, abort")
		return

	if !npc_state[choice]:
		print("this choice was disabled, abord")
		buttons[choice].disabled = true
		return
	
	#check if is right (top priority)
	if cur_dialog_path["right_choice"] == choice:#right choice was pickup
		right_choice()
		return
	elif cur_dialog_path["wrong_choice"]  == choice:
		wrong_choice()
		return

#if reach here: its not a good bye, its not wrong or right == it's a neutral choice. Disable and repeat last succesful message
	state = s_db["retry"]
	$reply/speech.visible_characters = 0
	typewriter_cursor = 0
	$reply/speech.text = str("generic_",database.npc[idx_npc]["class"],"_",choice,"_",randi() % generic_speech[choice])
	database.npc[idx_npc]["state"][choice] = false
	buttons[choice].disabled = true
	set_npc_face("normal")


#	print(cur_dialog_path["right_choice"])
#	print(cur_dialog_path["wrong_choice"])
	
	#check if is fake (bottom priority)


func right_choice():
	#if level 0, set the actual name
	#switch icon
	#increase ["state"]["level"] +1

	#check if ther's a next one
	for i in buttons:
		buttons[i].disabled = false
		database.npc[idx_npc]["state"][i] = true
	database.npc[idx_npc]["state"]["level"] += 1
	var level = database.npc[idx_npc]["state"]["level"]
	var dialogs = database.npc[idx_npc]["dialog_path"]
	for d in dialogs:
		if d["level"] == level:
			icon.play("next", d["icon"] )
#			print(d["icon"])
			break
	run_action()


func wrong_choice():
	for i in buttons:
		database.npc[idx_npc]["state"][i] = false
		buttons[i].disabled = true
	run_action()
	icon.play("burn")
	print("wrong choice was taken, measures...")

func failsafe_exit():
	print("failsafe exit")

func run_chatting():
	state = s_db["chatting"]

func _on_deceit_pressed():
	chose("deceit")


func _on_comply_pressed():
	chose("comply")


func _on_fright_pressed():
	chose("fright")


func _on_awe_pressed():
	chose("awe")


func _on_daunt_pressed():
	chose("daunt")


func _on_brace_pressed():
	chose("brace")


func _on_goodbye_pressed():
	chose("goodbye")
	pass # Replace with function body.
