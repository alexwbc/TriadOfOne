extends Container

const s_db = 	{0:"clean"
				,1:"discover"
				,2:"sending"
				,3:"wait_return"
				}

var state = 0
var active = false
var my_database = []
var my_active_elements = []
var my_elements = []
var my_id = 0
var my_current_data
var pinpointer
func disable():
	pass

func _enter_tree():
	pinpointer = preload("res://system/pinpoint.tscn")
	database.world = self

#	for i in range(database.world_map.size()):
#		print(database.world_map[i])
#	return
#	for i in database.world_map:
#		if i.has("id"):
#			if i["id"] == database.current_world:
#				$background.texture = load("res://maps/world/"+ str(database.current_world) + str(".webp"))
#				$world_name.text = i["name"]
#				return

func refresh(world_id):
	database.world.active = true
	database.current_world = world_id
	for i in database.world_map:
		if i["id"] == world_id:
			my_current_data =i
	print(my_current_data)
	var background = load("res://maps/world/"+str(world_id)+"_"+str(database.wheater)+ ".png")
	$world_name.text = str("this map is: ", my_current_data["name"])
	$background.texture = background
	my_id = world_id
	for n in ($pinpoints.get_children()):
		n.queue_free()

	my_database = []
	my_active_elements = []


	my_elements = []
	for i in database.world_elements.size():
		var element = database.world_elements[i]
		if element["home"] == my_id:
			my_database.append(i)
			if element["active"]:
				my_active_elements.append(element)
				var placing = pinpointer.instance()
				placing.data = element
				$pinpoints.add_child(placing)

#	for i in database.world_elements:
#		if (i["home"] == my_id) and i["active"]:
#			my_elements.append(i)
#			var placing = pinpointer.instance()
#			placing.data = i
#			$pinpoints.add_child(placing)


#	print(my_elements)
	var main = database.main
	if main.state == main.s_db["run_world"]:
		main.state = main.s_db["ready"]


func check_pinpoint(request):#try to activate pinpoints

	for idx in my_database:
		var check = database.world_elements[idx]
		if !check["active"]:
			var activate = false
			if check["main_req"] == "any" or check["main_req"] == request:
				 activate = true
			
			if activate:
				var placing = pinpointer.instance()
				placing.data = check
				placing.created = true
				$pinpoints.add_child(placing)
				database.world_elements[idx]["active"] = true




func _on_int_pressed():
	check_pinpoint("int")
	
func _on_dex_pressed():
	check_pinpoint("dex")
	
func _on_str_pressed():
	check_pinpoint("str")
	


func _on_int_mouse_exited():
	$heroes/int.release_focus()

func _on_dex_mouse_exited():
	$heroes/dex.release_focus()


func _on_str_mouse_exited():
	$heroes/str.release_focus()
